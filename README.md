## node-red-contrib-common-transformations

Some common transformations of payloads so these can easily be used by dragging
them in a flow and wiring them up, instead of having to create 'Function' nodes
and copy-paste the same Javascript code many times.
