/**
 * Some reusable functions to transform KNX output
 * into input values for other components.
 */

module.exports = function(RED) {

  /**
   * Function to filter out valid slider values.
   * Case: somehow, the status link of a KNX Dimmer emits the original
   * KNX value. Don't know how, because value is already changed before it gets to the link,
   * but still it happens.
   * Since a slider will reset to the 'min' value when it receives an invalid value,
   * this causes the slider not to display the actual value.
   */
  function FilterValidSliderValues(config) {
    RED.nodes.createNode(this, config);
    var node = this

    /*
     * Convert KNX msg: { payload: { value: <number>, oldval: [number,null]}}
     * to switch value: { payload: <number>}
     */
    node.on('input', function(msg) {
      if (isNaN(msg.payload)) {
        node.debug('filter: ignoring '+JSON.stringify(msg));
        // ignore msg
      } else {
        node.debug('filter: passing '+JSON.stringify(msg));
        node.send(msg);
      }
    });
  }

  /**
   * Function to filter any input.
   * Only matching message will pass the filter.
   */
  function FilterFunction(config) {
    var vm = require("vm");

    RED.nodes.createNode(this, config);
    var node = this;

    var context = vm.createContext({}); // empty context
    var filterFunction = config.filterFunction;

    try {
      node.script = vm.createScript(filterFunction);

      /*
       * Convert KNX msg: { payload: { value: <number>, oldval: [number,null]}}
       * to switch value: { payload: <number>}
       */
      node.on('input', function(msg) {
        context.msg = msg;
        var result = node.script.runInContext(context);
        node.log("result: "+ result);

        if (Boolean(result)) {
          node.debug('filter: passing '+JSON.stringify(msg));
          node.send(msg);
        } else {
          node.debug('filter: ignoring '+JSON.stringify(msg));
        }
      });
    }
    catch(err) {
      node.warn("Error: "+ err);
    }

  }

  RED.nodes.registerType("filter-valid-slider-values", FilterValidSliderValues);
  RED.nodes.registerType("filter-function", FilterFunction);
}
