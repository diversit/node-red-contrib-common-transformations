/**
 * Some reusable functions to transform KNX output
 * into input values for other components.
 */

module.exports = function(RED) {
  /**
   * Transformation to convert a payload with a boolean 'value' property.
   * E.g. {'payload': { 'value': 1}} becomes {'payload': true}.
   *
   * This function can be used to convert KNX-SwitchDevice output payload
   * to a UI-Switch input payload.
   */
  function TransformBooleanValueToPayload(config) {
    RED.nodes.createNode(this, config);
    var node = this;

    /*
     * Convert (KNX) msg: { payload: { value: [0,1,true,false], oldval: [0,1,true,false]}}
     * to switch value: { payload: [true,false]}
     */
    node.on('input', function(msg) {
      node.debug('transforming: '+JSON.stringify(msg));
      msg.payload = Boolean(msg.payload.value);
      node.send(msg);
    });
  }

  /**
   * Transformation to convert a payload with a number 'value' property.
   * E.g. {'payload': { 'value': 100}} becomes {'payload': 100}.
   *
   * This function can be used to convert KNX-DimmerDevice output payload
   * to a UI-Slider input payload.
   */
  function TransformNumberValueToPayload(config) {
    RED.nodes.createNode(this, config);
    var node = this;

    /*
     * Convert (KNX) msg: { payload: { value: <number>, oldval: [number,null]}}
     * to switch value: { payload: <number>}
     */
    node.on('input', function(msg) {
      node.debug('transforming: '+JSON.stringify(msg));
      msg.payload = msg.payload.value;
      node.send(msg);
    });
  }

  RED.nodes.registerType("transform-boolean-value", TransformBooleanValueToPayload);
  RED.nodes.registerType("transform-number-value", TransformNumberValueToPayload);
}
